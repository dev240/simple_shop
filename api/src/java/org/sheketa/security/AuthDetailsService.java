package org.sheketa.security;

import org.sheketa.Util.UserNotFoundException;
import org.sheketa.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
@Transactional
public class AuthDetailsService implements UserDetailsService {
    @Autowired
    UserDao userDao;
    public UserDetails loadUserByUsername(String username){
        return userDao.findByUsername(username);
    }

}

