package org.sheketa.security;


import org.sheketa.Util.JwtAuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthProvider implements AuthenticationProvider {

    @Autowired
    JwtService jwtService;

    public Authentication authenticate(Authentication jwtAuthToken) throws AuthenticationException {
        try {
            JwtAuthProfile authProfile = jwtService.validateToken(jwtAuthToken);
            return authProfile;
        } catch (Exception e) {
            throw new JwtAuthenticationException("Bad credentials", e);
        }
    }

    public boolean supports(Class<?> aClass) {
        return JwtAuthToken.class.equals(aClass);
    }
}
