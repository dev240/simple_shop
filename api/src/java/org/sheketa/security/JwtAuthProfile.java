package org.sheketa.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;


public class JwtAuthProfile implements Authentication {

    private UserDetails userDetails;

    public JwtAuthProfile(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userDetails.getAuthorities();
    }

    public String getName() {
        return userDetails.getUsername();
    }

    public Object getCredentials() {
        return userDetails.getPassword();
    }

    public Object getDetails() {
        return null;
    }

    public Object getPrincipal() {
        return userDetails;
    }

    public boolean isAuthenticated() {
        return true;
    }

    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }
}
