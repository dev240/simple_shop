package org.sheketa.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.sheketa.Util.JwtAuthenticationException;
import org.sheketa.Util.TokenParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {"classpath:application.properties"})
@Component
public class JwtService {

    @Autowired
    Environment environment;
    @Autowired
    UserDetailsService userDetailsService;

    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";

    private String secret = "mySecret";

    private Long expiration = (long) 604800;

    public String generateToken(LoginCredentials credentials) {
        String token = "";
        if (credentials.getUsername() == null || credentials.getPassword() == null)
            return token;
        UserDetails userDetails = userDetailsService.loadUserByUsername(credentials.getUsername());
        if (!credentials.getPassword().equals(userDetails.getPassword()))
            return token;
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    public JwtAuthProfile validateToken(Authentication jwtAuthToken) throws Exception {
        String token = (String) jwtAuthToken.getCredentials();
        String userName = getUsernameFromToken(token);

        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
        checkUser(userDetails);
        if(checkUser(userDetails) && !isTokenExpired(token)) {
            JwtAuthProfile jwtAuthProfile = new JwtAuthProfile(userDetails);
            return jwtAuthProfile;
        }
        else throw new BadCredentialsException("Bad credentials");
    }

    public String getUsernameFromToken(String token) throws TokenParseException {
        try {
            final Claims claims = getClaimsFromToken(token);
            return claims.getSubject();
        } catch (Exception e) {
            throw new TokenParseException("Cannot parse token");        }
    }

    public Date getCreatedDateFromToken(String token) {
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
        } catch (Exception e) {
            created = null;
        }
        return created;
    }
    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    public boolean checkUser(UserDetails userDetails){
        return (userDetails.isAccountNonLocked()&& userDetails.isEnabled()
                && userDetails.isCredentialsNonExpired() && userDetails.isCredentialsNonExpired());
    }


}
