package org.sheketa.security;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthToken implements Authentication {

    private final String authToken;

    public JwtAuthToken(String authToken){
        this.authToken = authToken;
    }
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getName() {
        return null;
    }

    public Object getCredentials() {
        return authToken;
    }

    public Object getDetails() {
        return null;
    }

    public Object getPrincipal() {
        return null;
    }

    public boolean isAuthenticated() {
        return false;
    }

    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }
}
