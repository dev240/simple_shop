package org.sheketa.security;

import org.sheketa.Util.NotUniqueUsernameException;
import org.sheketa.model.CandidateUser;
import org.sheketa.model.User;
import org.sheketa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
public class LoginService {

    @Autowired
    JwtService jwtService;
    @Autowired
    UserService userService;
    @Autowired
    UserDetailsService userDetailsService;

    public String signUp(CandidateUser candidateUser) throws NotUniqueUsernameException{
        if(isUserUnique(candidateUser)){
            userService.createUser(candidateUser);
            UserDetails userDetails = userDetailsService.loadUserByUsername(candidateUser.getUsername());
            return jwtService.generateToken(userDetails);
        }
        else throw new NotUniqueUsernameException("User exist");
    }

    public String signIn(LoginCredentials loginCredentials) {
        UserDetails userDetails = identifyUser(loginCredentials);
        return jwtService.generateToken(userDetails);
    }

    private UserDetails identifyUser(LoginCredentials credentials) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(credentials.getUsername());
        if (userDetails!=null && credentials.getPassword().equals(userDetails.getPassword())){
            return userDetails;
        }
        else throw new BadCredentialsException("Bad credentials");
    }
    private Boolean isUserUnique(CandidateUser candidateUser){
        return userService.findUser(candidateUser.getUsername())==null && userService.findEmail(candidateUser.getEmail())==null;
    }

}