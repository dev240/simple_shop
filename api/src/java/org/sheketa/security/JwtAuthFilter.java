package org.sheketa.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthFilter implements Filter{
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        String authHeader = servletRequest.getHeader("Authorization");
        if (authHeader != null) {
            try {
                JwtAuthToken token = new JwtAuthToken(authHeader.replaceAll("Bearer ", ""));
                SecurityContextHolder.getContext().setAuthentication(token);
            }
            catch (Exception e){
            }
        }
        chain.doFilter(request, response);
    }

    public void destroy() {
    }

}
