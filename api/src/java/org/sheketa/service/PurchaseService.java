package org.sheketa.service;


import org.sheketa.model.Card;
import org.sheketa.model.CartItem;
import org.sheketa.model.Product;
import org.sheketa.model.Purchase;

import java.io.ByteArrayOutputStream;
import java.util.List;

public interface PurchaseService {
    Purchase createPurchase(List<CartItem> cartItems);
    String createWithdraw(Card card);
    ByteArrayOutputStream getPdfInvoiceOutputStream();
}
