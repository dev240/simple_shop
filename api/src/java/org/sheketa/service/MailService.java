package org.sheketa.service;

import org.sheketa.model.Purchase;

import java.io.File;

public interface MailService {
    void sendEmail(Purchase purchase, File invoice);
}
