package org.sheketa.service;

import org.sheketa.model.CandidateUser;
import org.sheketa.model.User;

import java.util.List;

public interface UserService {
    User findUser(String name);
    User findUser(Long id);
    User findEmail(String email);
    List<User> findUsersByParams(String search, String param, String sortBy, String order, int limit, int page);
    Long countTotalUsersInSearch(String search, String param);
    List<User> findAllUsers();
    void createUser(CandidateUser user);
    void updateUser(User user);
    void deleteUser(User user);
    void lockUser(User user);
    boolean isExist(User user);
    User getAuthenticatedUser();
}
