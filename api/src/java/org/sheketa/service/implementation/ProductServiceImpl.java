package org.sheketa.service.implementation;

import org.sheketa.dao.ProductDao;
import org.sheketa.model.CartItem;
import org.sheketa.model.Product;
import org.sheketa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductDao productDao;

    public void createProduct(Product product) {
        productDao.save(product);
    }

    public void updateProduct(Product product) {
        productDao.update(product);
    }

    public void deleteProduct(Product product) {
        productDao.delete(product);
    }

    public Product findProductById(Long id) {
        return productDao.findById(id);
    }

    public List<Product> findProductsByParameters(String name, String category, String sortBy, String orderBy, int limit, int page) {
        return productDao.findByParameters(name, category, sortBy, orderBy, limit, page);
    }

    public List<Product> findAllProducts() {
        return productDao.findAll();
    }

    public Boolean isProductExist(Product product) {
        return productDao.exists(product);
    }

    public void rateProduct(Product product, Integer mark) {
        BigDecimal currentRate = product.getRate();
        Long rates = product.getRates();
        BigDecimal newRate = currentRate.multiply(
                BigDecimal.valueOf(rates)).add(BigDecimal.valueOf(mark))
                .divide(BigDecimal.valueOf(rates + 1), 2, BigDecimal.ROUND_HALF_UP);
        product.setRate(newRate);
        this.updateProduct(product);
    }

    public Long countTotalProductsInSearch(String name, String category) {
        return productDao.countTotalProductsInSearch(name, category);
    }

    public List<Product> checkAvailability(List<CartItem> cartItems) {
        return productDao.checkAvailability(cartItems);
    }

    public void decreaseProductsAfterWithdraw(List<CartItem> cartItems) {
        for (CartItem item : cartItems) {
            Product product = item.getProduct();
            product.setQuantity(product.getQuantity() - item.getQuantity());
            productDao.update(product);
        }
    }

    public void lockProduct(Product product) {
        product.setNonBlocked(!product.getNonBlocked());
        this.updateProduct(product);
    }

}

