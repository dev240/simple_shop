package org.sheketa.service.implementation;

import org.sheketa.Util.BankException;
import org.sheketa.model.Card;
import org.sheketa.service.BankService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;

//Bank mock;
@Service("bankService")
public class BankServiceImpl implements BankService {
    private static String shopBankAccount="1234567890987654321";
    public String createWithdraw(Card card, BigDecimal totalSum) throws BankException{
        //bank withdraw simulation
        if(Math.random()*10==1){
            throw new BankException("Withdraw by card "+card.getNumber() + "failed");
        }
        else{
            return "Withdraw by card "+card.getNumber() + "successful";
        }
    }
}
