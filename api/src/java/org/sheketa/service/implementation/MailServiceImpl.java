package org.sheketa.service.implementation;


import org.sheketa.model.Purchase;
import org.sheketa.service.MailService;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.io.File;


@Service("mailService")
public class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSender mailSender;

    public void sendEmail(Purchase purchase, File invoice) {

        MimeMessagePreparator preparator = getContentWithAttachementMessagePreparator(purchase, invoice);

        try {
            mailSender.send(preparator);
        } catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private MimeMessagePreparator getContentWithAttachementMessagePreparator(final Purchase purchase, final File invoice) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

                helper.setSubject("Shop order placement");
                helper.setFrom("customerserivces@yourshop.com");
                helper.setTo(purchase.getUser().getEmail());
                String content = "Dear " + purchase.getUser().getFirstName()+" "+purchase.getUser().getLastName()
                        + ", thank you for placing order. Your order id is " + purchase.getId() + ".";

                helper.setText(content);

                helper.addAttachment("invoice.pdf", invoice);
            }
        };
        return preparator;
    }
}