package org.sheketa.service.implementation;

import org.sheketa.dao.RoleDao;
import org.sheketa.dao.UserDao;
import org.sheketa.model.CandidateUser;
import org.sheketa.model.Role;
import org.sheketa.model.User;
import org.sheketa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    public User findUser(String name) {
        System.out.println("name"+name);
        return userDao.findByUsername(name);
    }

    public User findUser(Long id) {
        return userDao.findById(id);
    }

    public User findEmail(String email) {
        return userDao.findByEmail(email);
    }

    public List<User> findUsersByParams(String search, String param, String sortBy, String order, int limit, int page) {
        return userDao.findByParams(search, param, sortBy, order, limit, page);
    }

    public Long countTotalUsersInSearch(String search, String param) {
        return userDao.countTotalUsersInSearch(search, param);
    }

    public List<User> findAllUsers() {
        return userDao.findAll();
    }

    public void updateUser(User user) {
        userDao.update(user);
    }

    public void deleteUser(User user) {
        userDao.delete(user);
    }

    public void createUser(User user) {
        userDao.save(user);
    }

    public void createUser(CandidateUser candidateUser) {
        User user = new User();
        user.setUsername(candidateUser.getUsername());
        user.setPassword(candidateUser.getPassword());
        user.setEmail(candidateUser.getEmail());
        user.setFirstName(candidateUser.getFirstName());
        user.setLastName(candidateUser.getLastName());

        Set<Role> roles = new HashSet<Role>();
        Role role = roleDao.findByName("ROLE_USER");
        roles.add(role);
        user.setRoles(roles);
        userDao.save(user);
    }

    public void lockUser(User user) {
       user.setAcountNonLocked(!user.isAccountNonLocked());
        updateUser(user);
    }

    public boolean isExist(User user) {
        return findUser(user.getUsername())!=null;
    }

    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User authenticatedUser = findUser(authentication.getName());
        return authenticatedUser;
    }

}

