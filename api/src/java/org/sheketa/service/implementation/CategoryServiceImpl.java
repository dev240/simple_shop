package org.sheketa.service.implementation;

import org.sheketa.dao.CategoryDao;
import org.sheketa.model.Category;
import org.sheketa.model.Product;
import org.sheketa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryDao categoryDao;

    public Boolean isCategoryExist(Category category) {
        return categoryDao.exists(category);
    }

    public List<Category> findAllCategories() {
        return categoryDao.findAll();
    }

    public void createCategory(Category category) {
        if(!isCategoryExist(category))
        categoryDao.save(category);
    }

    public void updateCategory(Category category) {
        categoryDao.update(category);
    }

    public void deleteProduct(Category category) {
        categoryDao.delete(category);
    }
}
