package org.sheketa.service.implementation;


import org.sheketa.Util.BankException;
import org.sheketa.Util.PdfCreator;
import org.sheketa.dao.CartItemDao;
import org.sheketa.dao.ProductDao;
import org.sheketa.dao.PurchaseDao;
import org.sheketa.model.Card;
import org.sheketa.model.CartItem;
import org.sheketa.model.Purchase;
import org.sheketa.model.User;
import org.sheketa.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service("purchaseService")
@Transactional
public class PurchaseServiceImpl implements PurchaseService {

    @Autowired
    PurchaseDao purchaseDao;
    @Autowired
    ProductService productService;
    @Autowired
    UserService userService;
    @Autowired
    BankService bankService;
    @Autowired
    MailService mailService;

    private static final String TEMP_DIR = "C:\\my_temp";

    public Purchase createPurchase(List<CartItem> cartItems) {

        User authenticatedUser = userService.getAuthenticatedUser();
        BigDecimal totalSum = calculateTotalSum(cartItems);
        System.out.println("User" + authenticatedUser.toString());
        Purchase purchase = new Purchase();
        purchase.setStatus("ACTIVE");
        purchase.setUser(authenticatedUser);
        purchase.setCartItems(cartItems);
        purchase.setTotalSum(totalSum);
        System.out.println(purchase.toString());
        purchaseDao.save(purchase);
        return purchase;
    }


    public String createWithdraw(Card card) {
        User authenticatedUser = userService.getAuthenticatedUser();
        Purchase purchase = purchaseDao.findActivePurchaseByUser(authenticatedUser);
        if (purchase == null) {
            return "NO_PURCHASES";
        }
        try {
            bankService.createWithdraw(card, purchase.getTotalSum());
            purchase.setCloseDate(new Date());
            purchase.setStatus("CLOSED");
            purchaseDao.update(purchase);
            PdfCreator.createPDF(purchase, TEMP_DIR + "\\" + "Order_" + purchase.getId()+".pdf");
            mailService.sendEmail(purchase, new File(TEMP_DIR + "\\" + "Order_" + purchase.getId()+".pdf"));
            productService.decreaseProductsAfterWithdraw(purchase.getCartItems());
            return "SUCCESSFUL";
        } catch (BankException e) {
            purchase.setStatus("FAILED");
            purchaseDao.update(purchase);
            return "FAILED";
        }
    }

    private BigDecimal calculateTotalSum(List<CartItem> items) {
        BigDecimal totalSum = BigDecimal.ZERO;
        for (CartItem item : items) {
            totalSum = totalSum.add(item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
        }
        return totalSum;
    }


    public ByteArrayOutputStream getPdfInvoiceOutputStream() {
        User user = userService.getAuthenticatedUser();
        System.out.println(user.getUsername());
        System.out.println("UUUUUUUUUUUUUUUUU"+user);
        String fileName = TEMP_DIR + "\\" + "Order_" + purchaseDao.getLastPurchaseByUser(user).getId()+".pdf";
        //String fileName = "C:\\my_temp\\Order_32.pdf";
        try {
            ByteArrayOutputStream baos = convertPDFToByteArrayOutputStream(fileName);
            return baos;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private ByteArrayOutputStream convertPDFToByteArrayOutputStream(String fileName) {

        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            inputStream = new FileInputStream(fileName);
            byte[] buffer = new byte[1024];
            baos = new ByteArrayOutputStream();

            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return baos;
    }
}
