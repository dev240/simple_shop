package org.sheketa.service;

import org.sheketa.Util.BankException;
import org.sheketa.model.Card;

import java.math.BigDecimal;

public interface BankService {
    String createWithdraw(Card card, BigDecimal totalSum) throws BankException;
}
