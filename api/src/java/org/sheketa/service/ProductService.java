package org.sheketa.service;


import org.sheketa.model.CartItem;
import org.sheketa.model.Category;
import org.sheketa.model.Product;

import java.awt.peer.ListPeer;
import java.util.List;

public interface ProductService {
    void createProduct(Product product);
    void updateProduct(Product product);
    void deleteProduct(Product product);
    Product findProductById(Long id);
    List<Product> findProductsByParameters(String name, String category, String sortBy, String orderBy, int limit, int page);
    List<Product> findAllProducts();
    void rateProduct(Product product, Integer mark);
    void lockProduct(Product product);
    Boolean isProductExist(Product product);
    Long countTotalProductsInSearch(String name, String category);
    List<Product>checkAvailability(List<CartItem> cartItems);
    void decreaseProductsAfterWithdraw(List<CartItem> cartItems);





   // void addViewToProduct(Long id);
 //   List<Product> findProductByCategory(Category category);
//    List<Product> findProductByCategoryPaginate(String category, int results, int page);
//    List<Product> findAllProductsPaginate(int results, int page);
//    List<Product> findProductByNamePaginate(String name, int results, int page);
//    List<Product> findProductsByNameCategoryPaginate(String name, String category, int results, int page);
//    //void likeProduct(Product product);

}
