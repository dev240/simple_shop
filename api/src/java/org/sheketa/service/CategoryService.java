package org.sheketa.service;

import org.sheketa.model.Category;

import java.util.List;

public interface CategoryService {
    public List<Category> findAllCategories();
}
