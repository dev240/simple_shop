package org.sheketa.Util;


public class BankException extends Exception{
    public BankException(String message) {
        super(message);
    }
}
