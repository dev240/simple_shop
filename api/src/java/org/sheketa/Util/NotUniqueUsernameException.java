package org.sheketa.Util;


public class NotUniqueUsernameException extends Exception {
    public NotUniqueUsernameException(String message) {
        super(message);
    }
}
