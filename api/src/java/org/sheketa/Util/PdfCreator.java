package org.sheketa.Util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.sheketa.model.Purchase;
import org.springframework.stereotype.Component;

public class PdfCreator {
    private static Font TIME_ROMAN = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
    private static Font TIME_ROMAN_SMALL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    public static Document createPDF(Purchase purchase, String file) {
        Document document = null;
        try {
            document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();
            document.addTitle("PDF invoice");
            document.addSubject("invoice");
            document.addAuthor("Shop");
            document.addCreator("Shop");

            Paragraph preface = new Paragraph();
            creteEmptyLine(preface, 1);
            preface.add(new Paragraph("Shop", TIME_ROMAN));

            creteEmptyLine(preface, 1);
            preface.add(new Paragraph("Order: "+purchase.getId(), TIME_ROMAN_SMALL));
            preface.add(new Paragraph("Customer: "+purchase.getUser().getFirstName()+" "+purchase.getUser().getLastName(), TIME_ROMAN_SMALL));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            preface.add(new Paragraph("Payment date "
                    + simpleDateFormat.format(new Date()), TIME_ROMAN_SMALL));
            document.add(preface);

            Paragraph paragraph = new Paragraph();
            creteEmptyLine(paragraph, 2);
            document.add(paragraph);
            PdfPTable table = new PdfPTable(4);

            PdfPCell c1 = new PdfPCell(new Phrase("n/n"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Product name"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Quantity"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Price"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            table.setHeaderRows(1);

            for (int i = 0; i < purchase.getCartItems().size(); i++) {
                table.setWidthPercentage(100);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                table.addCell(String.valueOf(i+1));
                table.addCell(purchase.getCartItems().get(i).getProduct().getName());
                table.addCell(String.valueOf(purchase.getCartItems().get(i).getQuantity()));
                table.addCell(purchase.getCartItems().get(i).getProduct().getPrice().toPlainString());
            }

            document.add(table);

            Paragraph total = new Paragraph();
            creteEmptyLine(total, 1);
            total.add(new Paragraph("Total sum: " + purchase.getTotalSum() + "UAH", TIME_ROMAN_SMALL));

            document.add(total);

            document.close();

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return document;

    }

    private static void creteEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
