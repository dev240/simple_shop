package org.sheketa.Util;

public class TokenParseException extends Exception {
    public TokenParseException(String message) {
        super(message);
    }
}
