package org.sheketa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(unique = true, nullable = false)
    private String email;

    @Column
    private String phone;

    @Column
    private String firstName;

    @Column
    private String lastName;


    @Column
    private Date dateOfRegistration = new Date();

    @Column(columnDefinition = "bit(1) default 1")
    private Boolean acountNonExpired = true;

    @Column(columnDefinition = "bit(1) default 1")
    private Boolean acountNonLocked = true;

    @Column(columnDefinition = "bit(1) default 1")
    private Boolean credentailsNonExpired = true;

    @Column(columnDefinition = "bit(1) default 1")
    private Boolean enabled = true;

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private List<Purchase> purchases;

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<Role> roles;

    public User() {
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

    public boolean isAccountNonExpired() {
        return acountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return acountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentailsNonExpired;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public Boolean getAcountNonExpired() {
        return acountNonExpired;
    }

    public void setAcountNonExpired(Boolean acountNonExpired) {
        this.acountNonExpired = acountNonExpired;
    }

    public Boolean getAcountNonLocked() {
        return acountNonLocked;
    }

    public void setAcountNonLocked(Boolean acountNonLocked) {
        this.acountNonLocked = acountNonLocked;
    }

    public Boolean getCredentailsNonExpired() {
        return credentailsNonExpired;
    }

    public void setCredentailsNonExpired(Boolean credentailsNonExpired) {
        this.credentailsNonExpired = credentailsNonExpired;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setAccoutisEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                '}';
    }
}
