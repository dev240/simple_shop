package org.sheketa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.*;


import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "purchases")
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date creationDate =new Date();

    @Column
    private Date closeDate;

    @Column(precision = 10, scale = 2)
    private BigDecimal totalSum;

    @Column
    private String status;

    @JsonBackReference
    @ManyToOne
    @JoinColumn
    private User user;


    @JsonManagedReference
    @OneToMany(mappedBy = "purchase", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<CartItem> cartItems = new ArrayList<CartItem>();


    public Purchase() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        for(CartItem item: cartItems){
            item.setPurchase(this);
        }
        this.cartItems = cartItems;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", creationDate=" + creationDate +
                ", closeDate=" + closeDate +
                ", totalSum=" + totalSum +
                ", status='" + status + '\'' +
                ", user=" + user +
                ", cartItems=" + cartItems +
                '}';
    }
}

