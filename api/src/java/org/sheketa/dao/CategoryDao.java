package org.sheketa.dao;

import org.sheketa.model.Category;

import java.util.List;

public interface CategoryDao {
    List<Category> findAll();
    List<Category> findByName(String name);
    void save(Category category);
    void delete(Category category);
    void update(Category category);
    Boolean exists(Category category);
}
