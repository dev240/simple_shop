package org.sheketa.dao;

import org.sheketa.model.Role;

import java.util.List;

public interface RoleDao{
    List<Role> findAll();
    Role findByName(String name);
}
