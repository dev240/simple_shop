package org.sheketa.dao;


import org.sheketa.model.CartItem;

public interface CartItemDao {
    void save(CartItem cartItem);
    void update(CartItem cartItem);
    void delete(CartItem cartItem);
}
