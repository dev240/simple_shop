package org.sheketa.dao.implementation;

import org.sheketa.dao.AbstractDao;
import org.sheketa.dao.CategoryDao;
import org.sheketa.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("categoryDao")
public class CategoryDaoImpl extends AbstractDao<Long, Category> implements CategoryDao {

    @SuppressWarnings("unchecked")
    public List<Category> findByName(String name) {
        List<Category> categories = getEntityManager()
                .createQuery("SELECT c FROM Category c WHERE c.name LIKE CONCAT('%',:name,'%') ORDER BY c.name ASC")
                .setParameter("name", name)
                .getResultList();
        return categories;
    }

    @SuppressWarnings("unchecked")
    public List<Category> findAll() {
        List<Category> categories = getEntityManager()
                .createQuery("SELECT c FROM Category c ORDER BY c.name ASC")
                .getResultList();
        return categories;
    }

    public void save(Category category) {
        super.persist(category);
    }

    public void delete(Category category) {
        super.delete(category);
    }

    public void update(Category category) {
        super.update(category);
    }

    public Boolean exists(Category category) {
        try {
            getEntityManager()
                    .createQuery("SELECT c FROM Category c WHERE c.name = :name")
                    .setParameter("name", category.getName())
                    .getResultList();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }
}
