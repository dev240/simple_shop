package org.sheketa.dao.implementation;

import org.sheketa.dao.AbstractDao;
import org.sheketa.dao.PictureDao;
import org.sheketa.model.Picture;
import org.springframework.stereotype.Repository;

@Repository
public class PictureDaoImpl extends AbstractDao<Long, Picture> implements PictureDao {
    public void save(Picture picture) {
        super.persist(picture);
    }

    public void update(Picture picture) {
        super.update(picture);
    }

    public void delete(Picture picture) {
        super.delete(picture);
    }
}
