package org.sheketa.dao.implementation;

import org.sheketa.dao.AbstractDao;
import org.sheketa.dao.PurchaseDao;
import org.sheketa.model.Purchase;
import org.sheketa.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("purchaseDao")
public class PurchaseDaoImpl extends AbstractDao<Long, Purchase> implements PurchaseDao {
    public void save(Purchase purchase) {
        getEntityManager().createNativeQuery("DELETE cart_items FROM cart_items INNER JOIN purchases ON " +
                "cart_items.purchase_id = purchases.id WHERE purchases.user_id = ?1 AND purchases.status ='ACTIVE'")
                .setParameter(1, purchase.getUser().getId())
                .executeUpdate();
        getEntityManager().createQuery("DELETE FROM Purchase p WHERE p.user = :user AND p.status = :status")
                .setParameter("user", purchase.getUser())
                .setParameter("status", "ACTIVE")
                .executeUpdate();
        super.persist(purchase);
    }

    public void update(Purchase purchase) {
        super.update(purchase);
    }

    public Purchase findActivePurchaseByUser(User user) {
        try{
            Purchase purchase = (Purchase)getEntityManager()
                    .createQuery("SELECT p FROM Purchase p WHERE p.user = :user AND p.status = :status")
                    .setParameter("user", user)
                    .setParameter("status", "ACTIVE")
                    .getSingleResult();
            return purchase;
        }
        catch (NoResultException e){
            return null;
        }
    }

    public Purchase getLastPurchaseByUser(User user) {
        try{
            List<Purchase> purchases = getEntityManager()
                    .createQuery("SELECT p FROM Purchase p WHERE p.user = :user ORDER BY p.id DESC")
                    .setParameter("user", user)
                    .getResultList();
            return purchases.get(0);
        }
        catch (NoResultException e){
            return null;
        }
    }

    public void delete(Purchase purchase){
        super.delete(getEntityManager().contains(purchase) ? purchase : getEntityManager().merge(purchase));
    }
}
