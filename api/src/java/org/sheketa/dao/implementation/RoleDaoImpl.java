package org.sheketa.dao.implementation;


import org.sheketa.dao.AbstractDao;
import org.sheketa.dao.RoleDao;
import org.sheketa.model.Product;
import org.sheketa.model.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("roleDao")
public class RoleDaoImpl extends AbstractDao<Long, Role> implements RoleDao {

    @SuppressWarnings("unchecked")
    public List<Role> findAll() {
        List<Role> roles = getEntityManager()
                .createQuery("SELECT r FROM Role r ORDER BY r.id ASC")
                .getResultList();
        return roles;
    }

    public Role findByName(String name) {
        Role role = (Role) getEntityManager()
                .createQuery("SELECT r FROM Role r WHERE r.name = :name")
                .setParameter("name", name)
                .getSingleResult();
        return role;
    }
}
