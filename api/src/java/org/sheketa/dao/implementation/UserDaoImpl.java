package org.sheketa.dao.implementation;


import org.sheketa.dao.AbstractDao;
import org.sheketa.dao.UserDao;
import org.sheketa.model.Product;
import org.sheketa.model.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Long, User> implements UserDao {

    public User findByUsername(String username) {
        try {
            User user = (User) getEntityManager()
                    .createQuery("SELECT u FROM User u WHERE u.username = :username")
                    .setParameter("username", username)
                    .getSingleResult();
            return user;
        } catch (NoResultException ex) {
            return null;
        }
    }

    public User findById(Long id) {
        return getByKey(id);
    }

    public User findByEmail(String email) {
        try {
            User user = (User) getEntityManager()
                    .createQuery("SELECT u FROM User u WHERE u.email = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            return user;
        } catch (NoResultException e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> findByParams(String search, String param, String sortBy, String order, int limit, int page) {
        List<User> users = getEntityManager()
                //.createQuery("SELECT u FROM User u WHERE u.name LIKE 'iv' ORDER BY u.name DESC")
                .createQuery("SELECT u FROM User u " +
                        "WHERE u." + param + " LIKE CONCAT('%',:search,'%') " +
                        "ORDER BY" + " u." + sortBy + " " + order)
                .setParameter("search", search)
                .setMaxResults(limit)
                .setFirstResult(page * limit)
                .getResultList();
        return users;
    }

    public Long countTotalUsersInSearch(String search, String param) {
        try {
            Long count = (Long) getEntityManager()
                    .createQuery("SELECT COUNT(u) FROM User u " +
                            "WHERE u." + param + " LIKE CONCAT('%',:search,'%')")
                    .setParameter("search", search)
                    .getSingleResult();
            return count;
        } catch (NoResultException e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        List<User> users = getEntityManager()
                .createQuery("SELECT u FROM User u ORDER BY u.id ASC")
                .getResultList();
        return users;
    }

    public void save(User user) {
        persist(user);
    }

    public void update(User user) {
        super.update(user);
    }

    public void delete(User user) {
        super.delete(getEntityManager().contains(user) ? user : getEntityManager().merge(user));
    }
}
