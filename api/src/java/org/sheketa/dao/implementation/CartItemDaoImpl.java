package org.sheketa.dao.implementation;

import org.sheketa.dao.AbstractDao;
import org.sheketa.model.CartItem;
import org.sheketa.model.Product;
import org.springframework.stereotype.Repository;

@Repository("cartItemDao")
public class CartItemDaoImpl extends AbstractDao<Long, CartItem> {
    public void save(CartItem cartItem) {
        super.persist(cartItem);
    }

    public void update(CartItem cartItem) {
        super.update(cartItem);
    }

    public void delete(CartItem cartItem) {
        super.delete(getEntityManager().contains(cartItem) ? cartItem : getEntityManager().merge(cartItem));
    }

}
