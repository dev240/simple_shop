package org.sheketa.dao.implementation;

import org.sheketa.dao.AbstractDao;
import org.sheketa.dao.ProductDao;
import org.sheketa.model.CartItem;
import org.sheketa.model.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao<Long, Product> implements ProductDao {
    public void save(Product product) {
        super.persist(product);
    }

    public void update(Product product) {
        super.update(product);
    }

    public void delete(Product product) {
        super.delete(getEntityManager().contains(product) ? product : getEntityManager().merge(product));
    }

    public Product findById(Long id) {
        return getByKey(id);
    }

    @SuppressWarnings("unchecked")
    public List<Product> findByParameters(String name, String category, String sortBy, String order, int limit, int page) {
        System.out.println(name + category + sortBy + order + limit + page);
        List<Product> products = getEntityManager()
                .createQuery("SELECT p FROM Product p " +
                        "WHERE p.name LIKE CONCAT('%',:name,'%') " +
                        "AND p.category.name LIKE CONCAT('%',:category,'%') " +
                        "ORDER BY" + " p." + sortBy + " " + order)
                .setParameter("name", name)
                .setParameter("category", category)
                .setMaxResults(limit)
                .setFirstResult(page * limit)
                .getResultList();
        return products;
    }

    public Long countTotalProductsInSearch(String name, String category) {
        try {
            Long count = (Long) getEntityManager()
                    .createQuery("SELECT COUNT(p) FROM Product p " +
                            "WHERE p.name LIKE CONCAT('%',:name,'%') " +
                            "AND p.category.name LIKE CONCAT('%',:category,'%')")
                    .setParameter("name", name)
                    .setParameter("category", category)
                    .getSingleResult();
            return count;
        } catch (NoResultException e) {
            return null;
        }
    }

    public Product findByBarcode(Long barcode) {
        try {
            Product products = (Product) getEntityManager()
                    .createQuery("SELECT p FROM Product p WHERE p.barcode = :barcode")
                    .setParameter("barcode", barcode)
                    .getSingleResult();
            return products;
        } catch (NoResultException e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Product> findAll() {
        List<Product> products = getEntityManager()
                .createQuery("SELECT p FROM Product p")
                .getResultList();
        return products;
    }

    public Boolean exists(Product product) {
        if (findByBarcode(product.getBarcode()) != null) {
            return true;
        }
        return false;
    }

    public List<Product> checkAvailability(List<CartItem> cartItems) {
        List<Product> absentProducts = new ArrayList<Product>();
        for (CartItem item : cartItems) {
            Product currentProduct = item.getProduct();
            if (currentProduct.getQuantity() < item.getQuantity()) {
                absentProducts.add(currentProduct);
            }
        }
        return absentProducts;
    }
}
