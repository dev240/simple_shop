package org.sheketa.dao;


import org.sheketa.model.CartItem;
import org.sheketa.model.Product;

import java.util.List;

public interface ProductDao {
    void save(Product product);
    void update(Product product);
    void delete(Product product);
    Product findById(Long id);
    List<Product> findByParameters(String name, String category, String sortBy, String order, int limit, int page);
    Long countTotalProductsInSearch(String name, String category);
    List<Product> findAll();
    List<Product> checkAvailability(List<CartItem> cartItems);
    Boolean exists(Product product);
}
