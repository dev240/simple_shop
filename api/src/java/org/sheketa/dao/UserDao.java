package org.sheketa.dao;


import org.sheketa.model.User;

import java.util.List;

public interface UserDao {
    User findByUsername(String username);
    User findById(Long id);
    User findByEmail(String email);
    List<User>findByParams(String search, String param, String sortBy, String order, int limit, int page);
    Long countTotalUsersInSearch(String search, String param);
    List<User> findAll();
    void save(User user);
    void update(User user);
    void delete(User user);
}
