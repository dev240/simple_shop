package org.sheketa.dao;

import org.sheketa.model.Purchase;
import org.sheketa.model.User;

public interface PurchaseDao {
    Purchase findActivePurchaseByUser(User user);
    Purchase getLastPurchaseByUser(User user);
    void save(Purchase purchase);
    void update(Purchase purchase);
    void delete(Purchase purchase);


}
