package org.sheketa.controller;

import org.apache.log4j.Logger;
import org.sheketa.Util.NotUniqueUsernameException;
import org.sheketa.model.CandidateUser;
import org.sheketa.model.User;
import org.sheketa.security.LoginCredentials;
import org.sheketa.security.LoginService;
import org.sheketa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    LoginService loginService;
    @Autowired
    UserService userService;

    private Logger logger = Logger.getLogger(LoginController.class);

    @RequestMapping(value = "/sign_up", method = RequestMethod.POST)
    public ResponseEntity signUp(@RequestBody CandidateUser candidateUser) {
        try {
            String token = loginService.signUp(candidateUser);
            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.put("Token", Arrays.asList(token));
            System.out.println("user created");
            User user = userService.findUser(candidateUser.getUsername());
            logger.info("User sign up successful");
            return new ResponseEntity<User>(user, headers, HttpStatus.OK);
        } catch (NotUniqueUsernameException e) {
            logger.error("User with current credentials exists");
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/sign_in", method = RequestMethod.POST)
    public ResponseEntity signIn(@RequestBody LoginCredentials loginCredentials) {
        try {
            User user = userService.findUser(loginCredentials.getUsername());
            if (!user.getAcountNonLocked()){
                return new ResponseEntity(HttpStatus.LOCKED);
            }
            String token = loginService.signIn(loginCredentials);
            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.put("Token", Arrays.asList(token));
            logger.info("User sign in successful");
            return new ResponseEntity<User>(user, headers, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            logger.info("Bad credentials");
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ResponseEntity getUserAccount() {
        try {
            User user = userService.getAuthenticatedUser();
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }

}
