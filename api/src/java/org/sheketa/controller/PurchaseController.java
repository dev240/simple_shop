package org.sheketa.controller;

import org.apache.log4j.Logger;
import org.sheketa.Util.BankException;
import org.sheketa.Util.PdfCreator;
import org.sheketa.model.Card;
import org.sheketa.model.CartItem;
import org.sheketa.model.Product;
import org.sheketa.service.ProductService;
import org.sheketa.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PurchaseController {

    @Autowired
    ProductService productService;
    @Autowired
    PurchaseService purchaseService;

    private Logger logger = Logger.getLogger(PurchaseController.class);

    @RequestMapping(value = "/purchase/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity createPurchase(@RequestBody List<CartItem> items) {
        if (items.size() == 0) {
            logger.error("Cannot create purchase.List of items is empty");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        List<Product> wantedProducts = productService.checkAvailability(items);
        if (!wantedProducts.isEmpty()) {
            logger.error("There are not enough products in the store");
            return new ResponseEntity<List<Product>>(wantedProducts, HttpStatus.NOT_MODIFIED);
        }
        logger.info("Purchase successful");
        purchaseService.createPurchase(items);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/purchase/withdraw", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity createWithdraw(@RequestBody Card card) {
        if (purchaseService.createWithdraw(card).equals("SUCCESSFUL")) {
            logger.info("Withdraw successful");
            return new ResponseEntity(HttpStatus.OK);
        }
        if (purchaseService.createWithdraw(card).equals("NO_PURCHASES")) {
            logger.error("Withdraw failed");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        logger.error("Withdraw failed");
        return new ResponseEntity(HttpStatus.NOT_MODIFIED);
    }

    @RequestMapping(value = "/purchase/download_invoice", method = RequestMethod.GET)
    public void downloadPDF(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=" + "invoice.pdf");
        response.setContentType("application/pdf");
        try {
            ByteArrayOutputStream baos = purchaseService.getPdfInvoiceOutputStream();
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            logger.info("Invoice upload successful");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}

