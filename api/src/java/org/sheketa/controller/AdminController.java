package org.sheketa.controller;

import org.apache.log4j.Logger;
import org.sheketa.Util.CustomErrorType;
import org.sheketa.model.Product;
import org.sheketa.model.User;
import org.sheketa.service.ProductService;
import org.sheketa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    private Logger logger = Logger.getLogger(AdminController.class);

    /*U S E R   M A N A G E M E N T*/

    @RequestMapping(value = "/users",
            method = RequestMethod.GET,
            params = {"search", "param", "sortBy", "order", "limit", "page"})
    ResponseEntity getUsersByParameters(
            @RequestParam("search") String search,
            @RequestParam("param") String param,
            @RequestParam("sortBy") String sortBy,
            @RequestParam("order") String order,
            @RequestParam("limit") int limit,
            @RequestParam("page") int page) {
        List<User> users = userService.findUsersByParams(search, param, sortBy, order, limit, page);
        if (users.isEmpty()) {
            logger.info("No users found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        Long count = userService.countTotalUsersInSearch(search, param);
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Total-Results-Length", Arrays.asList(Long.toString(count)));
        logger.info("Retrieve users by params");
        return new ResponseEntity<List<User>>(users, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getAllUsers() {
        List<User> list = userService.findAllUsers();
        if (list.isEmpty()) {
            logger.error("List of users is empty");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        logger.info("Retrieve all users");
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    ResponseEntity getUser(@PathVariable("id") long id) {
        User user = userService.findUser(id);
        if (user == null) {
            logger.error("User with id " + id + "not found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        logger.info("Retrieve user with id " + id);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    ResponseEntity updateUser(@PathVariable("id") long id) {
        User user = userService.findUser(id);
        if (user == null) {
            logger.error("Cannot update user.User with id " + id + "not found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        userService.updateUser(user);
        logger.info("User with id " + id + "was successfully updated");
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    ResponseEntity deleteUser(@PathVariable("id") long id) {
        User user = userService.findUser(id);
        if (user == null) {
            logger.error("Cannot delete user.User with id " + id + "not found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        userService.deleteUser(user);
        logger.info("User with id " + id + "was successfully deleted");
        return new ResponseEntity<User>(HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{id}/lock", method = RequestMethod.GET)
    ResponseEntity lockUser(@PathVariable("id") long id) {
        User user = userService.findUser(id);
        if (user == null) {
            logger.error("Cannot lock user.User with id " + id + "not found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        userService.lockUser(user);
        logger.info("User with id " + id + "was successfully locked");
        return new ResponseEntity(HttpStatus.OK);
    }

    /* P R O D U C T   M A N A G E M E N T*/

    @RequestMapping(value = "/products",
            method = RequestMethod.GET,
            params = {"name", "category", "sortBy", "order", "limit", "page"})
    ResponseEntity getProductsByParameters(
            @RequestParam("name") String name,
            @RequestParam("category") String category,
            @RequestParam("sortBy") String sortBy,
            @RequestParam("order") String order,
            @RequestParam("limit") int limit,
            @RequestParam("page") int page) {
        if (category.equals("All Categories"))
            category = "";
        List<Product> products = productService.findProductsByParameters(name, category, sortBy, order, limit, page);
        if (products.isEmpty()) {
            logger.error("List of products is empty");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        Long count = productService.countTotalProductsInSearch(name, category);
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Total-Results-Length", Arrays.asList(Long.toString(count)));
        logger.info("Retrieve products by params");
        return new ResponseEntity<List<Product>>(products, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/products", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> createProduct(@RequestBody Product product) {
        if (productService.isProductExist(product)) {
            logger.error("Cannot create product.There is another product with current id");
            return new ResponseEntity<Object>(HttpStatus.CONFLICT);
        }
        productService.createProduct(product);
        logger.info("Product was successfully created");
        return new ResponseEntity<Object>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody Product product) {
        Product foundProduct = productService.findProductById(id);
        if (foundProduct == null) {
            logger.error("Cannot update product.Product with id " + id + "not found");
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        }
        productService.updateProduct(product);
        logger.info("Product with id " + id + "was successfully updated");
        return new ResponseEntity<Product>(HttpStatus.OK);
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteProduct(@PathVariable("id") long id) {
        System.out.println("id" + id);
        Product product = productService.findProductById(id);
        if (product == null) {
            logger.error("Cannot delete product.Product with id " + id + "not found");
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        }
        productService.deleteProduct(product);
        logger.info("Product with id " + id + "was successfully deleted");
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @RequestMapping(value = "/products/{id}/lock", method = RequestMethod.GET)
    ResponseEntity lockProduct(@PathVariable("id") long id) {
        Product product = productService.findProductById(id);
        if (product == null) {
            logger.error("Cannot lock product.Product with id " + id + "not found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        productService.lockProduct(product);
        logger.info("Product with id " + id + "was successfully locked");
        return new ResponseEntity(HttpStatus.OK);
    }
}
