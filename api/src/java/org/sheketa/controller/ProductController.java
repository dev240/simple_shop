package org.sheketa.controller;

import org.apache.log4j.Logger;
import org.sheketa.model.Category;
import org.sheketa.model.Product;
import org.sheketa.service.CategoryService;
import org.sheketa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductService productService;
    @Autowired
    CategoryService categoryService;

    private Logger logger = Logger.getLogger(ProductController.class);

    @RequestMapping(value = "/products",
            method = RequestMethod.GET,
            params = {"name", "category", "sortBy", "order", "limit", "page"})
    ResponseEntity getProductsByParameters(
            @RequestParam("name") String name,
            @RequestParam("category") String category,
            @RequestParam("sortBy") String sortBy,
            @RequestParam("order") String order,
            @RequestParam("limit") int limit,
            @RequestParam("page") int page) {
        if (category.equals("All Categories"))
            category = "";
        List<Product> products = productService.findProductsByParameters(name, category, sortBy, order, limit, page);
        if (products.isEmpty()) {
            logger.error("List of products is empty");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        Long count = productService.countTotalProductsInSearch(name, category);
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Total-Results-Length", Arrays.asList(Long.toString(count)));
        logger.info("Retrieve users by parameters");
        return new ResponseEntity<List<Product>>(products, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> getAllProducts() {
        List<Product> products = productService.findAllProducts();
        if (products.isEmpty()) {
            logger.info("List of products is empty");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        logger.info("Retrieve all products");
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> getProductById(@PathVariable("id") long id) {
        Product product = productService.findProductById(id);
        if (product == null) {
            logger.error("Product with id " + id + " not found.");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        logger.info("Retrieve product with id " + id + " successful");
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "/products/{id}/rate", method = RequestMethod.GET, params = "mark")
    ResponseEntity rateProduct(@PathVariable("id") Long id,
                               @RequestParam("mark") Integer mark) {
        Product product = productService.findProductById(id);
        if (product == null) {
            logger.error("Product with id " + " not found");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        if (mark > 5) {
            logger.error("Cannot rate product with current mark");
            return new ResponseEntity(HttpStatus.NOT_MODIFIED);
        }
        System.out.println("N");
        productService.rateProduct(product, mark);
        logger.info("Rate product with id" + id + "with mark" + mark);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> getAllCategories() {
        List<Category> categories = categoryService.findAllCategories();
        if (categories.isEmpty()) {
            logger.error("List of categories is empty");
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        logger.info("Retrieve all categories");
        return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);
    }
}
