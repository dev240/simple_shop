
INSERT INTO categories VALUES (NULL,"Phones");
INSERT INTO categories VALUES (NULL,"Tabs");
INSERT INTO categories VALUES (NULL,"Notebooks");
INSERT INTO categories VALUES (NULL,"Headphones");
INSERT INTO categories VALUES (NULL,"TVs");
INSERT INTO categories VALUES (NULL,"Walkmans");

INSERT INTO roles VALUES (NULL,"ROLE_USER");
INSERT INTO roles VALUES (NULL,"ROLE_ADMIN");

INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('admin@gmail.com', 'John', 'Smith', 'adminpass', '093-850-12-18', 'admin');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('petro.smereka@gmail.com', 'Petro', 'Smereka', 'password2', '093-850-75-18', 'petro');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('annaS@gmail.com', 'Anna', 'Suhetska', 'asdfhjkl', '063-458-04-18', 'savana93');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('iryna_kuz@gmail.com', 'Iryna', 'Kuzyk', 'ghjfk454545', '093-000-05-18', 'irynka');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('olgapater@gmail.com', 'Olga', 'Pater', 'portici123', '093-858-00-18', 'olganik');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('kovalyshyn@gmail.com', 'Marta', 'Koval', 'knopka333', '093-456-05-18', 'knopka34');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('kushnirstp@gmail.com', 'Stepan', 'Kushnir', 'grfbgrvdfgd', '093-858-89-56', 'kushnirlh');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('dzuba@gmail.com', 'Anelia', 'Dzuba', '1234dfgdf', '096-858-11-11', 'dzubar');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('john@gmail.com', 'John', 'Doe', 'qqqqwwwweee', '097-800-11-18', 'john_doe');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('katuha@gmail.com', 'Kateryna', 'Matviychuk', 'werwertwertwe', '098-978-63-96', 'katka');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('fallen_angel@gmail.com', 'Valentin', 'Munshtuk', 'fdgdfgdgf', '098-118-13-96', 'fallen_angel');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('yakunovski@gmail.com', 'Yan', 'Yakunovskiy', '1234fffff13', '093-444-55-23', 'yakunov');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('ulianius@gmail.com', 'Ulian', 'Vyshynskiy', '12fgfd', '069-789-123-18', 'zesar');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('vasyl@gmail.com', 'Vasyl', 'Babiy', '12341rtyryt3', '093-111-11-12', 'vasya');
INSERT INTO users (`email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES ('metrics114@gmail.com', 'User', 'User', 'userpass', '093-111-11-16', 'user');

INSERT INTO users_roles VALUES(1,2);
INSERT INTO users_roles VALUES(1,1);
INSERT INTO users_roles VALUES(2,1);
INSERT INTO users_roles VALUES(3,1);
INSERT INTO users_roles VALUES(4,1);
INSERT INTO users_roles VALUES(5,1);
INSERT INTO users_roles VALUES(6,1);
INSERT INTO users_roles VALUES(7,1);
INSERT INTO users_roles VALUES(8,1);
INSERT INTO users_roles VALUES(9,1);
INSERT INTO users_roles VALUES(10,1);
INSERT INTO users_roles VALUES(11,1);
INSERT INTO users_roles VALUES(12,1);
INSERT INTO users_roles VALUES(13,1);
INSERT INTO users_roles VALUES(14,1);
INSERT INTO users_roles VALUES(15,1);

INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('3454354353', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'iPhone 5', 1, '4999', '49', '5.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('3343434332', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'iPhone 5s', 1, '7129', '79', '4.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('7847834834', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'iPhone 6s', 1, '13299', '19', '4.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('8411323121', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'iPhone 6', 1, '11395', '15', '3.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('4565123812', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'iPhone 5se', 1, '12000', '10', '3.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('7894563131', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Samsung Galaxy S7', 1, '26000', '44', '4.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('7256532323', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Samsung Galaxy S6', 1, '24995', '44', '5.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('7545454543', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Samsung Galaxy S7 EDGE', 1, '25995', '4', '5.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('7845565655', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Samsung Galaxy Note', 1, '25000', '20', '3.0', '34', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('7865656565', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lumia 950 XL', 1, '44543', '33', '1.2', '17630', '1');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('34898080893', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'ASUS N55', 0, '4545', '40', '4.3', '4000', '3');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('31232434545', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'HP Elite Book', 1, '3500', '44', '5.0', '35000', '3');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('45453423232', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'MacBook Air', 1, '32095', '496', '4.1', '32095', '3');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('43434343434', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'iPad Mini', 1, '15299', '8', '2.2', '15299', '2');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('34343434343', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Marshall Major 2', 1, '1995', '54', '5.0', '1995', '4');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('43434343434', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Sehnheiser 3', 1, '2500', '4', '3.0', '2500', '4');
INSERT INTO `shopdb2`.`products` (`barcode`, `description`, `name`, `nonBlocked`, `price`, `quantity`, `rate`, `rates`, `category_id`) VALUES ('43434343434', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Koss Porta Pro', 0, '680', '4', '4.0', '680', '4');

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'http://hotline.ua/img/tx/734/7343315.jpg', 1);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'http://data.whicdn.com/images/63514542/original.jpg', 1);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'https://thumbs.dreamstime.com/z/apple-iphone-5-white-27155362.jpg', 1);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcndr1-_9T_tlE9rXrGk20-iTUmrVqHRfDGzTqOK4XJL8d14iK', 1);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'http://images-cdn.azureedge.net/azure/in-resources/e0af9c17-737e-4a5e-9202-38a44fb838d3/Images/ProductImages/Source/iphone5s.jpg;width=1000', 2);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'https://media.carphonewarehouse.com/is/image/cpw2/apple_iphone5s_hero_cc001?$JPG6Column$', 2);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'https://n1.sdlcdn.com/imgs/b/g/j/iPhone-5S-16-GB-Space-SDL218647224-5-d29e2.jpg', 2);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://stylus.ua/thumbs/320x320/6e/a0/428998.jpeg', 3);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'http://i2.rozetka.ua/goods/1335538/apple_iphone_6s_plus_128gb_space_gray_images_1335538209.jpg', 3);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'http://millonphonecenter.com/wp-content/uploads/2016/10/iPhone6-Slv-Angulos.jpg', 4);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'https://9to5mac.files.wordpress.com/2014/09/iphone6-select-2014.png?w=782', 4);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://cdn.macrumors.com/article-new/2016/02/iphonese-800x686.jpg?retina', 5);
INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (0, 'http://www.arashmobile.com/c/129-category_default/%D8%A2%DB%8C%D9%81%D9%88%D9%86-5se.jpg', 5);


INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'http://s7d2.scene7.com/is/image/SamsungUS/600_005_Galaxy_S7_bk_Right_Angle?$product-details-jpg$', 6);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'http://images.samsung.com/is/image/samsung/in-galaxy-s6-g920i-sm-g920izdains-030-front-gold?$PD_GALLERY_JPG$', 7);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://www.att.com/catalog/en/skus/Samsung/Galaxy%20S7%20Edge/overview/304969-pdp-overview-samsung-gs7_edge-tile_06.jpg', 8);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, NULL , 9);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, NULL , 10);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://www.notebookcheck-ru.com/fileadmin/_migrated/pics/ASUS-N55S-Total_5_03.jpg', 11);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjERTzgXuCTqR411h3HkYqUxW0nw2bkk09wv8kVCOHG2fMJRML', 12);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://stylus.ua/thumbs/320x320/ec/8e/457196.jpeg', 13);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://www.bhphotovideo.com/images/images2500x2500/apple_mk9p2ll_a_128gb_ipad_mini_4_1185480.jpg', 14);

INSERT INTO `shopdb2`.`pictures` (`isTitle`, `url`, `product_id`) VALUES (1, 'https://www.marshallheadphones.com/media/catalog/product/cache/52/thumbnail/522x/9df78eab33525d08d6e5fb8d27136e95/t/h/thumb__0005_marshall_major_ii_brown_rgb_highres_15.jpg.jpg', 15);









