import {Injectable, OnInit} from "@angular/core";
import {Http, Response, Headers, ResponseContentType, RequestOptions} from "@angular/http";
import {Product} from "../objects/Product";
import {Observable, BehaviorSubject} from "rxjs";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {CartItem} from "../objects/CartItem";
import {CardData} from "../objects/CardData";
import * as FileSaver from 'file-saver';


@Injectable()
export class ClientService {
  private SERVER_URL = "http://localhost:8080/Shop"

    private productsSource = new BehaviorSubject<Product[]>(null);
  public products = this.productsSource.asObservable();

  private pagesToPaginateSource = new BehaviorSubject<number>(0);
  public pagesToPaginate = this.pagesToPaginateSource.asObservable();

  private cartItemsSource = new BehaviorSubject<CartItem[]>(null);
  public cartItems = this.cartItemsSource.asObservable();

  nameParam = "";
  categoryParam = "";
  sortByParam = "rate";
  limitParam = 9;
  pageParam = 0;
  orderParam = "DESC";

  private anonHeaders = new Headers({
    'Content-Type': 'application/json',
  });


  constructor(private http: Http, private authService: AuthService, private router: Router) {
  }

  getProductsForUser() {
    this.loadProductsFromServer(this.nameParam, this.categoryParam, this.sortByParam,
      this.limitParam, this.pageParam, this.orderParam).subscribe(
      (products)=> {
        console.log(products);
        this.productsSource.next(products);
      },
      (error)=> {
        console.log("error");
        console.log(error);
      }
    )
  }

  loadProductsFromServer(name: string, category: string, sortBy: string, limit: number, page: number, order: string): Observable<Product[]> {
    console.log("inner" + name + category + limit + " " + page);
    return this.http.get(this.SERVER_URL +'/api/products' + '?name=' + name + '&category=' + category + '&sortBy=' + sortBy +
      '&order=' + order + '&limit=' + limit + '&page=' + page, {headers: this.anonHeaders})
      .map((response: Response)=> {
        console.log("recieved:" + response.json());
        var pages: number = Number(response.headers.get("Total-Results-Length"));
        this.pagesToPaginateSource.next(pages);
        console.log(response.json() + '232');
        return response.json() as Product[]
      })
      .catch((error: any)=> {
        console.log(error.status);
        return Observable.throw(error);
      });
  }

  paginateProducts(limit: number, page: number) {
    this.loadProductsFromServer(this.nameParam, this.categoryParam,
      this.sortByParam, limit, page, "DESC").subscribe(
      (products)=> {
        console.log(products);
        this.productsSource.next(products);
      },
      (error)=> {
        console.log("error");
        console.log(error);
      }
    )
  }

  getProductById(id: number): Observable<Product> {
    return this.http.get(this.SERVER_URL +'/api/products/' + id, {headers: this.anonHeaders})
      .map((resp: Response)=> {
        console.log(resp.json());
        return resp.json() as Product
      })
      .catch((error: any)=> {
        return Observable.throw(error);
      });
  }


  addProductToCart(product: Product) {
    var currentItems: CartItem[] = [];
    this.cartItems.subscribe(cartItems => currentItems = cartItems);
    if (currentItems != null) {
      if (this.productExistInCart(product)) {
        this.increaseProductInCart(product);
        return;
      }
    }
    else {
      currentItems = [];
    }
    currentItems.push(new CartItem(product, 1));
    this.cartItemsSource.next(currentItems);
  }

  removeProductFromCart(product: Product) {
    var currentItems: CartItem[] = [];
    this.cartItems.subscribe(cartItems => currentItems = cartItems);
    for (var i = 0; i < currentItems.length; i++) {
      if (currentItems[i].product.name == product.name) {
        currentItems.splice(i, 1)
      }
    }
    this.cartItemsSource.next(currentItems);
  }

  increaseProductInCart(product: Product) {
    var currentItems: CartItem[] = [];
    this.cartItems.subscribe(cartItems => currentItems = cartItems);
    for (var i = 0; i < currentItems.length; i++) {
      if (currentItems[i].product.name == product.name) {
        currentItems[i].quantity += 1;
      }
    }
    this.cartItemsSource.next(currentItems);
  }

  decreaseProductInCart(product: Product) {
    var currentItems: CartItem[] = [];
    this.cartItems.subscribe(cartItems => currentItems = cartItems);
    for (var i = 0; i < currentItems.length; i++) {
      if (currentItems[i].product.name == product.name) {
        if (currentItems[i].quantity > 1)
          currentItems[i].quantity -= 1;
        else {
          currentItems.splice(i, 1);
        }
      }
    }
    this.cartItemsSource.next(currentItems);
  }

  productExistInCart(product: Product): boolean {
    var currentItems: CartItem[] = [];
    this.cartItems.subscribe(cartItems => currentItems = cartItems);
    for (var i = 0; i < currentItems.length; i++) {
      if (currentItems[i].product.barcode == product.barcode) {
        return true
      }
    }
    return false;
  }
  createPurchase():Observable<any>{
    var currentItems: CartItem[] = [];
    this.cartItems.subscribe(cartItems => currentItems = cartItems);
    console.log("JJJJ"+ JSON.stringify(currentItems));
    return this.http.post(this.SERVER_URL +'/api/purchase/', JSON.stringify(currentItems), {headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response)=> {
        this.router.navigate(['landing/product-list/payment']);
        return response.json();
      })
      .catch((error: any)=> {
        console.log(error.status);
        return Observable.throw(error);
      });
  }

  createWithdraw(cardData: CardData):Observable<any> {
    return this.http.post(this.SERVER_URL +'/api/purchase/withdraw', JSON.stringify(cardData), {headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response)=> {
        this.cartItemsSource.next([]);
        return response.status;
      })
      .catch((error: any)=> {
        //console.log(error.status);
        return Observable.throw(error);
      });
  }
  downloadPdf(){
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer ' + this.authService.getToken(),
      'MyApp-Application' : 'AppName', 'Accept': 'application/pdf' });
    let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
    return this.http.get(this.SERVER_URL +'/api/purchase/download_invoice', options)
      .map(response=>{
    let fileBlob = response.blob();
        return new Blob([fileBlob], {
      type: 'application/pdf' // must match the Accept type;
    });
  })
  }
  rateProduct(product: Product, mark: number):Observable<any>{
    return this.http.get(this.SERVER_URL + '/api/products/'+product.id+'/rate?mark='+mark, {headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response)=> {
        return response.json();
      })
      .catch((error: any)=> {
        console.log(error.status);
        return Observable.throw(error);
      });
  }

}




