import {Injectable} from "@angular/core";
import {Http, Response, Headers} from "@angular/http";
import {User} from "../objects/User";
import 'rxjs/add/operator/toPromise';
import {Observable, BehaviorSubject} from "rxjs";
import {AuthService} from "./auth.service";
import {Product} from "../objects/Product";
@Injectable()
export class AdminService {

  private SERVER_URL = "http://localhost:8080/Shop";

  private usersSource = new BehaviorSubject<User[]>(null);
  public users = this.usersSource.asObservable();

  private pagesToPaginateSource = new BehaviorSubject<number>(0);
  public pagesToPaginate = this.pagesToPaginateSource.asObservable();

  private productsSource = new BehaviorSubject<Product[]>(null);
  public products = this.productsSource.asObservable();


  constructor(private http: Http, private authService: AuthService) {
  }

  /*    A D M I N     U S E R    S E R V I C E    */


  getUsersBySearchParam(search: string, param: string, sortBy: string, order: string, limit: number, page: number) {
    console.log("TEST");
    console.log("outer" + search + param + limit + " " + page);
    this.loadUsersFromServer(search, param, sortBy, order, limit, page).subscribe(
      (products)=> {
        console.log(products);
        this.usersSource.next(products);
        console.log("PTP" + this.pagesToPaginate)
      },
      (error)=> {
        console.log("error");
        console.log(error);
      }
    )
  }

  loadUsersFromServer(search: string, param: string, sortBy: string, order: string, limit: number, page: number): Observable<User[]> {
    console.log("inner" + search + +limit + " " + page);
    return this.http.get(this.SERVER_URL + '/api/admin/users' + '?search=' + search + '&param=' + param +
      '&sortBy=' + sortBy + '&order=' + order + '&limit=' + limit + '&page=' + page, {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response)=> {
        console.log("recieved:" + response.json());
        var pages: number = Number(response.headers.get("Total-Results-Length"));
        this.pagesToPaginateSource.next(pages);
        return response.json() as User[]
      })
      .catch((error: any)=> {
        console.log(error.status);
        return Observable.throw(error);
      });
  }

  lockUser(user: User): Observable<any> {
    return this.http.get(this.SERVER_URL + '/api/admin/users/' + user.id + '/lock', {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response)=> {;
        var status = response.status;

      })
      .catch((error: any)=> {
        return Observable.throw(error);
      });
  }


  getUserById(id: number): Observable < User > {
    return this.http.get(this.SERVER_URL + '/api/admin/users/' + id, {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((resp: Response)=> {
        console.log(resp.json());
        return resp.json() as User
      })
      .catch((error: any)=> {
        return Observable.throw(error);
      });
  }

  /*    A D M I N    P R O D U C T    S E R V I C E    */

  getProductsForAdmin(name: string, category: string, sortBy: string, limit: number, page: number, order: string) {
    this.loadProductsFromServer(name, category, sortBy, limit, page, order).subscribe(
      (products)=> {
        console.log(products);
        this.productsSource.next(products);
      },
      (error)=> {
        console.log("error");
        console.log(error);
      }
    )
  }

  loadProductsFromServer(name: string, category: string, sortBy: string, limit: number, page: number, order: string): Observable<Product[]> {
    console.log("inner" + name + category + limit + " " + page);
    return this.http.get(this.SERVER_URL +  '/api/admin/products' + '?name=' + name + '&category=' + category + '&sortBy=' + sortBy +
      '&order=' + order + '&limit=' + limit + '&page=' + page, {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response)=> {
        console.log("recieved:" + response.json());
        var pages: number = Number(response.headers.get("Total-Results-Length"));
        this.pagesToPaginateSource.next(pages);
        console.log(response.json() + '232');
        return response.json() as Product[]
      })
      .catch((error: any)=> {
        console.log(error.status);
        return Observable.throw(error);
      });
  }

  createProduct(product: Product): Observable <Product> {
    console.log("rrrr");
    console.log("NAME" + product.name);
    console.log("Prod" + JSON.stringify(product));
    return this.http.post(this.SERVER_URL +  '/api/admin/products', JSON.stringify(product), {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response) => {
        console.log(response.json());
        console.log("11");
        var status = response.status;
        console.log(status);
      })
      .catch((error)=> {
        console.log(error.json);
        console.log("2");
        var status = error.status;
        console.log(status);
        return Observable.throw(null);
      });
  }

  updateProduct(product: Product): Observable < Product > {
    console.log("rrrr");
    console.log("NAME" + product.name);
    console.log('EE' + '/api/admin/products/' + product.id + '/');
    console.log("Prod" + JSON.stringify(product));
    return this.http.put(this.SERVER_URL +  '/api/admin/products/' + product.id + '/', JSON.stringify(product), {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response) => {
        var status = response.status;
        console.log("Status" + status);
      })
      .catch((error)=> {
        var status = error.status;
        console.log(status);
        return Observable.throw(null);

      });
  }

  deleteProduct(product: Product): Observable<Product> {
    console.log("DEL");
    console.log("NAME" + product.name);
    console.log('EE' + '/api/admin/products/' + product.id + '/');
    console.log("Prod" + JSON.stringify(product));
    return this.http.delete(this.SERVER_URL + '/api/admin/products/' + product.id + '/', {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response) => {
        var status = response.status;
        console.log("Status" + status)
      })
      .catch((error)=> {
        var status = error.status;
        console.log(status);
        return Observable.throw(null);
      });
  }

  lockProduct(product: Product): Observable < Product > {
    console.log("rrrr");
    console.log("NAME" + product.name);
    return this.http.get(this.SERVER_URL +'/api/admin/products/' + product.id + '/lock', {headers:  new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken()
    })})
      .map((response: Response) => {
        console.log("status");
        var status = response.status;
        console.log("Status" + status);

      })
      .catch((error)=> {
        var status = error.status;
        console.log(status);
        return Observable.throw(null);
      });
  }
}


