import {Injectable} from "@angular/core";
import {Http, Response, Headers} from "@angular/http";
import {Product} from "../objects/Product";
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {Category} from "../objects/Category";

@Injectable()
export class CategoryService {

  private SERVER_URL = "http://localhost:8080/Shop";

  private headers = new Headers({
    'Content-Type': 'application/json',
  });


  constructor(private http: Http, private authService: AuthService, private router: Router) {

  }
  getAllCategories():Observable<Category[]> {
    return this.http.get(this.SERVER_URL + '/api/categories', this.headers)
      .map((resp:Response)=>{
        console.log(resp.json());
        return resp.json() as Category[];
      })
      .catch((error: any)=> {
        return Observable.throw(error);
      });
  }
}
