import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response, Headers, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {LoginCredentials} from "../objects/LoginCredentials";
import {User} from "../objects/User";
import {Profile} from "selenium-webdriver/firefox";
import {PossibleUser} from "../objects/PossibleUser";
import {BehaviorSubject} from "rxjs";

@Injectable()
export class AuthService {

  private SERVER_URL = "http://localhost:8080/Shop";

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this.getToken()
  });
  receivedHeader: string;

  constructor(private http: Http) {
  }

  signIn(credentials: LoginCredentials): Observable<User> {
    console.log(JSON.stringify(credentials));
    return this.http.post(this.SERVER_URL + '/api/sign_in', JSON.stringify(credentials), {
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
      .map((response: Response) => {
        console.log('HEADER  ' + response.headers.get('Token'));
        this.receivedHeader = response.headers.get('Token');
        localStorage.setItem('Token', this.receivedHeader);
        console.log(response.json());
        return response.json() as User;
      })
      .catch((error)=> {
        return Observable.throw(null);
      });
  }

  signUp(possibleUser: PossibleUser) {
    return this.http.post(this.SERVER_URL + '/api/sign_up', JSON.stringify(possibleUser), {
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
      .map((response: Response) => {
        this.receivedHeader = response.headers.get('Token');
        localStorage.setItem('Token', this.receivedHeader);
        return response.json() as User;
      })
      .catch((error)=> {
        return Observable.throw(error);
      });
  }
  logOut() {
    return localStorage.removeItem('Token');
  }

  getToken(): string {
    return localStorage.getItem('Token');
  }

  getAccount() {
    return this.http.get(this.SERVER_URL +'/api/account', {headers: this.headers})
      .map((response: Response)=> {
        console.log(response.json());
        return response.json() as User;
      })
      .catch((error: any)=> {
        console.log(error.status);
        return Observable.throw(error);
      });
  }

}
