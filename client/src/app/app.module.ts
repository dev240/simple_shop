import {BrowserModule} from '@angular/platform-browser';
import {NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './components/app.component/app.component';
import {LandingComponent} from './components/landing.component/landing.component';
import {MyFooter} from "./components/my-footer.component/my-footer.component";
import {MyHeaderComponent} from './components/my-header.component/my-header.component';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from './components/login.component/login.component';
import {InputTextModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {DataTableModule, SharedModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {AutoCompleteModule} from 'primeng/primeng';
import {TabMenuModule} from 'primeng/primeng';
import {RatingModule} from 'primeng/primeng';

import {AuthService} from "./services/auth.service";
import {ProductPageComponent} from './components/product-page.component/product-page.component';
import {ProductListComponent} from './components/product-list.component/product-list.component';
import {ProductListItemComponent} from './components/product-list-item.component/product-list-item.component';
import {DataListModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {LeftBarComponent} from './components/left-bar.component/left-bar.component'
import {CategoryService} from "./services/category.service";
import {SearchBarComponent} from './components/search-bar/search-bar.component';
import {AdminPageComponent} from './components/admin/admin-page.component/admin-page.component';
import {ProductManagementComponent} from './components/admin/product-management/product-management.component';
import {UserManagementComponent} from './components/admin/user-management/user-management.component';
import {OverlayPanelModule} from 'primeng/primeng';
import {GalleriaModule} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
import {InputMaskModule} from 'primeng/primeng';
import {SortingBarComponent} from './components/sort-bar.component/sorting-bar.component';
import {AdminService} from "./services/admin.service";
import {ClientService} from "./services/client.service";
import {CartComponent} from './components/cart.component/cart.component';
import { PaymentSimulationComponent } from './components/payment-simulation.component/payment-simulation.component';

const appRoutes: Routes = [
  {
    path: '', redirectTo: '/landing/product-list', pathMatch: 'full'},
  {
    path: 'landing', component: LandingComponent,
    children: [
      {path: '', redirectTo: 'product-list', pathMatch: 'full'},
      {path: 'product-list', component: ProductListComponent,},
      {path: 'product-list/product-page/:productId', component: ProductPageComponent,},
      {path: 'product-list/cart', component: CartComponent,},
      {path: 'product-list/payment', component: PaymentSimulationComponent,},
    ]
  },
  {
    path: 'login', component: LoginComponent},
  {
    path: 'admin', component: AdminPageComponent,
    children: [
      {path: '', redirectTo: 'product-management', pathMatch: 'full'},
      {path: 'product-management', component: ProductManagementComponent,},
      {path: 'user-management', component: UserManagementComponent,}
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    MyFooter,
    MyHeaderComponent,
    LoginComponent,
    ProductPageComponent,
    ProductListComponent,
    ProductListItemComponent,
    LeftBarComponent,
    SearchBarComponent,
    AdminPageComponent,
    ProductManagementComponent,
    UserManagementComponent,
    SortingBarComponent,
    CartComponent,
    PaymentSimulationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InputTextModule,
    ButtonModule,
    PasswordModule,
    DataListModule,
    PaginatorModule,
    DropdownModule,
    DataTableModule,
    SharedModule,
    OverlayPanelModule,
    DialogModule,
    GalleriaModule,
    InputTextareaModule,
    AutoCompleteModule,
    InputMaskModule,
    TabMenuModule,
    RatingModule,


    RouterModule.forRoot(appRoutes,{useHash: true})],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [AdminService, ClientService, AuthService, CategoryService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
