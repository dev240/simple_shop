import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {Category} from "../../objects/Category";
import {Router} from "@angular/router";
import {ClientService} from "../../services/client.service";

@Component({
  selector: 'left-bar',
  templateUrl: 'left-bar.component.html',
  styleUrls: ['left-bar.component.css']
})
export class LeftBarComponent implements OnInit {

  categories: Category[];

  constructor(private clientService: ClientService, private categoryService: CategoryService, private router: Router) {
  }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .subscribe((categories)=> {
        this.categories = categories;
      });
  }

  findProductsByCategory(category: Category) {
    this.router.navigate(['landing/product-list']);
    this.clientService.categoryParam = category.name;
    this.clientService.pageParam = 0;
    this.clientService.limitParam = 10;
    this.clientService.sortByParam = "rate";
    this.clientService.orderParam = "DESC";
    this.clientService.getProductsForUser();
  }

}
