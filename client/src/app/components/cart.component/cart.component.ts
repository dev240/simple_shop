import { Component, OnInit } from '@angular/core';
import {CartItem} from "../../objects/CartItem";
import {ClientService} from "../../services/client.service";
import {Subscription} from "rxjs";
import {Product} from "../../objects/Product";
import {RouterModule, Router} from "@angular/router";

@Component({
  selector: 'cart',
  templateUrl: 'cart.component.html',
  styleUrls: ['cart.component.css']
})
export class CartComponent implements OnInit {

  cartItems: CartItem[] = [];
  cartItemsSubscription: Subscription;
  totalPrice: number=0;
  displayAuthMessage=false;
  displayWantedMessage=false;
  wantedProducts: Product[]=[];
  constructor(private clientService: ClientService, private router: Router) {
  }

  ngOnInit() {
    this.cartItemsSubscription = this.clientService.cartItems
      .subscribe(cartItems => {
        this.cartItems = cartItems;
        this.totalPrice = this.countTotalPrice(cartItems);
      });
  }

  increaseProductInCart(product: Product){
    this.clientService.increaseProductInCart(product);
  }

  decreaseProductInCart(product: Product){
    this.clientService.decreaseProductInCart(product);
  }

  removeProductFromCart(product: Product){
    this.clientService.removeProductFromCart(product);
  }

  countTotalPrice(cartItem: CartItem[]): number{
    let totalPrice = 0;
    for(let i = 0; i < cartItem.length; i++){
      totalPrice+=cartItem[i].product.price*cartItem[i].quantity;
    }
    return totalPrice;
  }
  createPurchase(){
    this.wantedProducts=[];
    this.clientService.createPurchase().subscribe(
      response=> this.toPayment(),
      error=> {
        if(error.status == 302){
          this.displayAuthMessage=true;
        }
        if(error.status == 304){
          this.displayWantedMessage=true;
        }
      }
    );
  }
  toPayment(){
    this.router.navigate(['landing/product-list/payment']);
  }
  toProductPage(){
    this.router.navigate(['landing']);
  }

}
