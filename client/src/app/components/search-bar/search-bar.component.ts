import { Component, OnInit } from '@angular/core';
import {Category} from "../../objects/Category";
import {CategoryService} from "../../services/category.service";
import {SelectItem} from 'primeng/primeng'
import {Router} from "@angular/router";
import {ClientService} from "../../services/client.service";

@Component({
  selector: 'search-bar',
  templateUrl: 'search-bar.component.html',
  styleUrls: ['search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  productName="";

  categories: Category[] = [];
  filteredCategories: Category[] = [];
  category: Category;

  constructor(private clientService: ClientService, private categoryService: CategoryService, private router: Router) { }

  ngOnInit() {
    this.categoryService.getAllCategories().subscribe((categories)=>{
      this.categories=categories;
      this.category = new Category(0,"All Categories");
      this.categories.push(this.category);
      this.clientService.getProductsForUser();
    });
  }
  onSearchClick(){
    this.clientService.nameParam=this.productName;
    this.clientService.categoryParam=this.category.name;
    this.clientService.getProductsForUser()
    this.router.navigate(['landing/product-list']);

  }

  searchCategory(event){
    this.filteredCategories = [];
    for(let i = 0; i<this.categories.length; i++){
      let scope = this.categories[i];
      if(scope.name.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredCategories.push(scope);
      }
    }
  }

  handleCategoryDropdownClick() {
    this.filteredCategories = [];
    setTimeout(() => {
      this.filteredCategories = this.categories;
    }, 100)
  }
}
