import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'my-header',
  templateUrl: 'my-header.component.html',
  styleUrls: ['my-header.component.css']
})
export class MyHeaderComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }
  toCart(){
    this.router.navigate(['landing/product-list/cart'])
  }
  toLanding(){
    this.router.navigate(['landing']);
  }


}
