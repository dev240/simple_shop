import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'landing',
  templateUrl: 'landing.component.html',
  styleUrls: ['landing.component.css']
})
export class LandingComponent implements OnInit, OnDestroy {

  private routerSubscription;
  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }
}
