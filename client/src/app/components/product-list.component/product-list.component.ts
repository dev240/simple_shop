import {Component, OnInit} from '@angular/core';
import {Product} from "../../objects/Product";
import {Subscription} from "rxjs";
import {ClientService} from "../../services/client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'product-list',
  templateUrl: 'product-list.component.html',
  styleUrls: ['product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[];
  productSubscription: Subscription;

  pagesToPaginate: number;
  pagesSubscription: Subscription;
  error: any;

  constructor(private clientService: ClientService, private router: Router) {
  }

  ngOnInit() {
    this.productSubscription = this.clientService.products
      .subscribe(products => this.products = products);
    this.pagesSubscription = this.clientService.pagesToPaginate
      .subscribe(pagesToPaginate=>this.pagesToPaginate = pagesToPaginate);
  }

  paginate(event) {
    console.log(event.rows+ " "+ event.page);
    console.log(this.products);
    this.clientService.paginateProducts(event.rows, event.page);
  }

}
