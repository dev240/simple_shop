import {Component, OnInit} from '@angular/core';
import {User} from "../../../objects/User";
import {Product} from "../../../objects/Product";
import {SelectItem} from 'primeng/primeng';
import {Subscription} from "rxjs";
import {AdminService} from "../../../services/admin.service";

@Component({
  selector: 'user-management',
  templateUrl: 'user-management.component.html',
  styleUrls: ['user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  users: User[];
  usersSubscription: Subscription;

  pagesToPaginate: number;
  pagesSubscription: Subscription;

  display: boolean = false;

  params: SelectItem[] = [];

  search = "";
  selectedParam = "username";
  sortBy = "id";
  order = "DESC";
  limit = 10;
  page = 0;

  icon: string ="fa-toggle-down";

  constructor(private adminService: AdminService) {
    this.params = [];
    this.params.push({label: 'by Username', value: 'username'});
    this.params.push({label: 'by Last Name', value: 'lastName'});
    this.params.push({label: 'by First Name', value: 'firstName'});
    this.params.push({label: 'by Email', value: 'email'});
  }

  ngOnInit() {
    console.log("init");
    this.adminService.getUsersBySearchParam(this.search, this.selectedParam, this.sortBy, this.order, this.limit, this.page);
    this.usersSubscription = this.adminService.users
      .subscribe(users => this.users = users);
    this.pagesSubscription = this.adminService.pagesToPaginate
      .subscribe(pagesToPaginate=>this.pagesToPaginate = pagesToPaginate);
    this.icon = "fa-toggle-down"
  }

  searchClick() {
    this.adminService.getUsersBySearchParam(this.search, this.selectedParam, this.sortBy, this.order, this.limit, 0);
    this.page = 0;
    console.log("ptp2" + this.pagesToPaginate);
  }

  changeSort(sortBy:string){
    this.sortBy = sortBy;
    this.adminService.getUsersBySearchParam(this.search, this.selectedParam, this.sortBy, this.order, this.limit, 0)
  }
  changeOrder() {
    if (this.order === "DESC") {
      console.log("D" + this.order);
      this.order = "ASC";
      this.icon = "fa-toggle-up";
    }
    else {
      console.log("A" + this.order);
      this.order = "DESC";
      this.icon = "fa-toggle-down";
    }
    this.adminService.getUsersBySearchParam(this.search, this.selectedParam, this.sortBy, this.order, this.limit, 0)
  }
  paginate(event) {
    this.limit = event.rows;
    this.page = event.page;
    this.getUsers()
  }

  lockAccount(user: User) {
    this.adminService.lockUser(user).subscribe(user=>this.getUsers());
  }

  getUsers() {
    this.adminService.getUsersBySearchParam(this.search, this.selectedParam, this.sortBy, this.order, this.limit, this.page);
  }
}
