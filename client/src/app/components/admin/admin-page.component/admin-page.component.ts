import {Component, OnInit} from '@angular/core';
import {User} from "../../../objects/User";
import {Router} from "@angular/router";
import {MenuItem} from 'primeng/primeng';

@Component({
  selector: 'admin-page',
  templateUrl: 'admin-page.component.html',
  styleUrls: ['admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  private items: MenuItem[];

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.items = [
      {label: 'Stats', icon: 'fa-bar-chart'},
      {label: 'Calendar', icon: 'fa-calendar'}
    ]
  }

  toProductManagement() {
    this.router.navigate(['admin/product-management'])
  }

  toUserManagement() {
    this.router.navigate(['admin/user-management'])
  }

}
