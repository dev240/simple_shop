import {Component, OnInit} from '@angular/core';
import {Product} from "../../../objects/Product";
import {OverlayPanel} from 'primeng/primeng'
import {Subscription} from "rxjs";

import {Category} from "../../../objects/Category";
import {CategoryService} from "../../../services/category.service";
import {AdminService} from "../../../services/admin.service";
import {Picture} from "../../../objects/Picture";

@Component({
  selector: 'product-management',
  templateUrl: 'product-management.component.html',
  styleUrls: ['product-management.component.css']
})
export class ProductManagementComponent implements OnInit {

  productName = "";
  category: Category;
  sortBy = "id";
  order = "DESC";
  limit = 10;
  page = 0;

  categories: Category[] = [];
  filteredCategories: Category[] = [];

  products: Product[];
  productSubscription: Subscription;

  selectedProduct: Product;
  editedProduct: Product;

  pagesToPaginate: number;
  pagesSubscription: Subscription;

  icon: string = "fa-toggle-down";

  display: boolean = false;
  dialogHeader: string;
  createUpdateModeToggle: boolean;
  pictureField: string;

  constructor(private adminService: AdminService, private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.productSubscription = this.adminService.products
      .subscribe(products => this.products = products);
    this.pagesSubscription = this.adminService.pagesToPaginate
      .subscribe(pagesToPaginate=>this.pagesToPaginate = pagesToPaginate);

    this.categoryService.getAllCategories().subscribe((categories)=> {
      this.categories = categories;
      this.category = new Category(0, "All Categories");
      this.categories.push(this.category);

      this.icon = "fa-toggle-down";

      this.adminService.getProductsForAdmin(this.productName, this.category.name,
        this.sortBy, this.limit, this.page, this.order);
    });
  }

  onSearchClick() {
    this.adminService.getProductsForAdmin(this.productName, this.category.name,
      this.sortBy, this.limit, this.page, this.order);
  }

  searchCategory(event) {
    this.filteredCategories = [];
    for (let i = 0; i < this.categories.length; i++) {
      let category = this.categories[i];
      if (category.name.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredCategories.push(category);
      }
    }
  }

  handleCategoryDropdownClick() {
    this.filteredCategories = [];
    setTimeout(() => {
      this.filteredCategories = this.categories;
    }, 100)
  }

  paginate(event) {
    console.log(event.rows + " " + event.page);
    console.log(this.products);
    this.limit = event.rows;
    this.page = event.page;
    this.adminService.getProductsForAdmin(this.productName, this.category.name,
      this.sortBy, this.limit, this.page, this.order);

  }

  changeSort(sortBy: string) {
    this.adminService.getProductsForAdmin(this.productName, this.category.name,
      sortBy, this.limit, this.page, this.order);
  }

  changeOrder() {
    if (this.order === "DESC") {
      console.log("D" + this.order);
      this.order = "ASC";
      this.icon = "fa-toggle-up";
    }
    else {
      console.log("A" + this.order);
      this.order = "DESC";
      this.icon = "fa-toggle-down";
    }
    this.adminService.getProductsForAdmin(this.productName, this.category.name,
      this.sortBy, this.limit, this.page, this.order);
  }

  editProduct(product: Product) {
    this.dialogHeader = "Edit product";
    this.createUpdateModeToggle = false;
    this.selectedProduct = product;
    this.editedProduct = Object.assign({}, this.selectedProduct);
    this.pictureField = this.stringifyPictures(this.editedProduct.pictures);
    this.display = true;
  }


  createProduct() {
    this.editedProduct = new Product();
    this.dialogHeader = "Create product";
    this.createUpdateModeToggle = true;
    this.display = true;
  }

  confirm() {
    this.display = false;
    this.editedProduct.pictures = this.parsePictures(this.pictureField);
    if (this.createUpdateModeToggle) {
      this.adminService.createProduct(this.editedProduct).subscribe(product=>
        this.reloadSourceAfterChange());
    }
    else {
      this.adminService.updateProduct(this.editedProduct).subscribe(product=>
        this.reloadSourceAfterChange());
    }
  }

  parsePictures(pictureString: string): Picture[] {
    if (pictureString != "") {
      var pictures: Picture[] = [];
      var pictureUrls: string[] = pictureString.split(';', 10);
      pictures.push(new Picture(pictureUrls[0], true));
      for (var i = 1; i < pictureUrls.length; i++) {
        pictures.push(new Picture(pictureUrls[i], false));
      }
    }
    return pictures;
  }

  stringifyPictures(picture: Picture[]): string {
    let pictureString = "";
    if (picture != null) {
      for (var i = 0; i < picture.length; i++) {
        pictureString += picture[i].url + ";";
      }
    }
    return pictureString;
  }

  decline() {
    this.display = false;
    this.editedProduct = null;
  }

  onHide($event) {
    this.editedProduct = null;
  }


  deleteProduct(product: Product) {
    this.adminService.deleteProduct(product).subscribe(product=>
      this.reloadSourceAfterChange());
  }

  lockProduct(product: Product) {
    this.adminService.lockProduct(product).subscribe(product=>
      this.reloadSourceAfterChange());
  }

  reloadSourceAfterChange() {
    this.pictureField = "";
    this.adminService.getProductsForAdmin(this.productName, this.category.name,
      this.sortBy, this.limit, this.page, this.order)
  }
}
