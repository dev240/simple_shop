import {Component, OnInit, Input} from '@angular/core';
import {Product} from "../../objects/Product";
import {ClientService} from "../../services/client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'product-list-item',
  templateUrl: 'product-list-item.component.html',
  styleUrls: ['product-list-item.component.css']
})
export class ProductListItemComponent implements OnInit {
  @Input()
  product: Product;
  titleImageUrl: string = "http://www.bell.ca/Styles/wireless/all_languages/all_regions/catalog_images/iPhone_7/iPhone7_MatBlk_lrg1_en.png";
  constructor(private clientService: ClientService, private router: Router) {
  }

  ngOnInit() {
    if (this.product.pictures.length>0) {
      for (var i = 0; i < this.product.pictures.length; i++) {
        if (this.product.pictures[i].isTitle==true) {
          this.titleImageUrl = this.product.pictures[i].url;
        }
      }
    }
    else {
      this.titleImageUrl = "http://www.novelupdates.com/img/noimagefound.jpg";
    }
  }

  addProductToCart(product: Product) {
    this.clientService.addProductToCart(product)
  }
  toProductPage(product: Product){
    this.router.navigate(['landing/product-list/product-page/',product.id])
  }
}
