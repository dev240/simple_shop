import {Component, OnInit} from '@angular/core';
import {LoginCredentials} from "../../objects/LoginCredentials";
import {AuthService} from "../../services/auth.service";
import {User} from "../../objects/User";
import {PossibleUser} from "../../objects/PossibleUser";
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;

  passwordAgainField: string;


  displaySignInDialog: boolean = false;
  isWarningMessageInvisible: boolean = true;

  displaySignUpDialog: boolean = false;
  isPasswordMessageInvisible: boolean = true;

  isNotUniqueMessageInvisible: boolean =true;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.getAccount().subscribe(user=>this.user = user);
    console.log("V" + this.isPasswordMessageInvisible);
  }

  showSignInDialog() {
    this.displaySignInDialog = true;
  }

  showSignUpDialog() {
    this.displaySignUpDialog = true;
  }

  signIn() {
    var loginCredentials = new LoginCredentials(this.username, this.password);
    this.authService.signIn(loginCredentials).subscribe(
      user=> {
        this.user = user;
        this.displaySignInDialog = false;
        this.clearFields();
      },
      error=> {
        this.displaySignInDialog = true;
        this.isWarningMessageInvisible = false;
      });
  }


  signUp() {
    console.log("SIGN" + this.username);
    this.authService.signUp(new PossibleUser(this.username, this.password,
      this.email, this.firstName, this.lastName, this.phone)).subscribe(
      data=> {
        this.user = data;
        this.displaySignUpDialog = false;
        this.clearFields();
      },
      error=>{
        this.isNotUniqueMessageInvisible = false;
      }
    )
  }

  logOut() {
    this.user = null;
    this.authService.logOut();
    this.router.navigate([''])
  }


  toAdminPage() {
    this.router.navigate(['admin'])
  }

  isAdmin(): boolean {
    if (this.user != null) {
      for (let i = 0; i < this.user.roles.length; i++) {
        if (this.user.roles[i].name == 'ROLE_ADMIN') {
          return true
        }
      }
    }
    return false;
  }
  cancelModal(){
    this.displaySignUpDialog=false;
    this.displaySignInDialog=false;
    this.clearFields();
  }


  clearFields() {
    this.username = "";
    this.password = "";
    this.passwordAgainField ="";
    this.firstName = "";
    this.lastName = "";
    this.email = "";
    this.phone = "";
    this.isNotUniqueMessageInvisible=true;
    this.isWarningMessageInvisible=true;
    this.isPasswordMessageInvisible= true;
  }

}
