import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/primeng'
import {ClientService} from "../../services/client.service";

@Component({
  selector: 'sorting-bar',
  templateUrl: 'sorting-bar.component.html',
  styleUrls: ['sorting-bar.component.css']
})
export class SortingBarComponent implements OnInit {

  sortParams: SelectItem[];
  selectedSort: string;
  icon: string = "fa fa-arrow-down fa-lg";

  constructor(private clientService: ClientService) {
    this.sortParams = [];
    this.sortParams.push({label:'by Category', value:'category'});
    this.sortParams.push({label:'by Name', value:'name'});
    this.sortParams.push({label:'by Price', value:'price'});
    this.sortParams.push({label:'by Rate', value:'rate'});
  }

  ngOnInit() {
  }
  changeSortBy(){
    this.clientService.sortByParam=this.selectedSort;
    this.clientService.pageParam=0;
    this.clientService.getProductsForUser();
  }
  changeOrder() {
    if (this.clientService.orderParam == "DESC") {
      console.log("D" + this.clientService.orderParam);
      this.clientService.orderParam = "ASC";
      this.icon = "fa fa-arrow-up fa-lg";
    }
    else {
      console.log("A" + this.clientService.orderParam);
      this.clientService.orderParam = "DESC";
      this.icon = "fa fa-arrow-down fa-lg";
    }
    this.clientService.getProductsForUser();
  }


}
