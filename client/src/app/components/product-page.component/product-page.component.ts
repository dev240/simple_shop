import {Component, OnInit} from '@angular/core';
import {Product} from "../../objects/Product";
import {ActivatedRoute, Params} from "@angular/router";
import {Picture} from "../../objects/Picture";
import {ClientService} from "../../services/client.service";

@Component({
  selector: 'product-page',
  templateUrl: 'product-page.component.html',
  styleUrls: ['product-page.component.css']
})
export class ProductPageComponent implements OnInit {

  product: Product;
  showedPictures: any[] = [];
  error: any;
  msg: string;
  displayAuthMessage: boolean = false;

  constructor(private clientService: ClientService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.clientService.getProductById(+params['productId']))
      .subscribe(
        (product) => {
          this.product = product;
          this.insertPictures(product.pictures);

        },
        (error) => {
          this.error = error;
          console.log("status" + error.status);
        }
      );
  }

  insertPictures(pictures: Picture[]) {
    for (var i = 0; i < pictures.length; i++) {
      this.showedPictures.push({source: pictures[i].url});
    }
  }

  addProductToCart(product: Product) {
    this.clientService.addProductToCart(product);
  }

  handleRate(event) {
    this.clientService.rateProduct(this.product, event.value).subscribe(
      response=> this.msg = "You have rated " + event.value,
      error=> {
        if (error.status == 302) {
          this.displayAuthMessage = true;
        }
      }
    );
  }

}
