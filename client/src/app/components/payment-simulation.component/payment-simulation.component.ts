import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ClientService} from "../../services/client.service";
import {CardData} from "../../objects/CardData";
import * as FileSaver from 'file-saver';

@Component({
  selector: 'payment-simulation',
  templateUrl: 'payment-simulation.component.html',
  styleUrls: ['payment-simulation.component.css']
})
export class PaymentSimulationComponent implements OnInit {

  number: string;
  month: string;
  year: string;
  holder: string;
  cvv: string='123';//Hardcoded value to prevent real card data enter input
  displayPurchase = false;
  constructor(private clientService: ClientService, private router: Router) { }

  ngOnInit() {
  }
  toCart(){
    this.router.navigate(['landing/product-list/cart'])
  }
  toLanding(){
    this.router.navigate(['landing'])
  }
  confirmPayment(){
    this.clientService.createWithdraw(new CardData(
      this.number, this.month, this.year, this.holder, this.cvv)).subscribe(
        data=>{
          console.log(data);
          this.displayPurchase=true;
          console.log("DDD");
          this.displayPurchase=true;
        },
      error=>{
      }
    )
  }
  downloadPdf() {
    this.clientService.downloadPdf().subscribe(blob=> {
        let filename = 'mypdf.pdf';
        FileSaver.saveAs(blob, filename);
      }
    );
  }
}
