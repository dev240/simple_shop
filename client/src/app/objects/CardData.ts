export class CardData{
  number: string;
  month: string;
  year: string;
  holder: string;
  cvv: string;

  constructor(number: string, month: string, year: string, holder: string, cvv: string) {
    this.number = number;
    this.month = month;
    this.year = year;
    this.holder = holder;
    this.cvv = cvv;
  }
}
