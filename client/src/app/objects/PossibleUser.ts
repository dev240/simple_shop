export class PossibleUser{
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;

  constructor(username: string, password: string, email: string, firstName: string, lastName: string, phone: string) {
    this.username = username;
    this.password = password;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
  }
}
