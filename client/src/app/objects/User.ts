import {Role} from "./Role";
export class User{
  id:number;
  username:string;
  password:string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  dateOfRegistration: Date;
  acountNonLocked: boolean;
  roles: Role[];


  constructor(id: number, username: string,
              password: string, firstName: string, lastName: string,
              email: string, phone: string, dateOfRegistration: Date,
              acountNonLocked: boolean, roles: Role[]) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.dateOfRegistration = dateOfRegistration;
    this.acountNonLocked = acountNonLocked;
    this.roles = roles
  }

}
