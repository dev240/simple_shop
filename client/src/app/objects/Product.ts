import {Picture} from "./Picture";
import {Category} from "./Category";
export class Product{

  id:number;
  barcode: number;
  name: string;
  description: string;
  quantity: string;
  price: number;
  rate:number;
  rates: number;
  views: number;
  nonBlocked: boolean;
  category: Category;
  pictures: Picture[];


  constructor() {
  }

// constructor(id: number, barcode: number, name: string,
  //             description: string, quantity: string, price: number,
  //             rate: number, category: Category, pictures: PictureCont[]) {
  //   this.id = id;
  //   this.barcode = barcode;
  //   this.name = name;
  //   this.description = description;
  //   this.quantity = quantity;
  //   this.price = price;
  //   this.rate = rate;
  //   this.pictures = pictures;
  //   this.category = category;
  // }
}
