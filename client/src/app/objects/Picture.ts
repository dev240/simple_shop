export class Picture{

  url: string;
  isTitle: boolean;


  constructor(url: string, isTitle: boolean) {
    this.url = url;
    this.isTitle = isTitle;
  }
}
